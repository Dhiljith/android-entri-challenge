package com.diljith.dhiljith_entrichallenge.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.diljith.dhiljith_entrichallenge.data.MovieRepository
import com.diljith.dhiljith_entrichallenge.database.MovieDatabase
import com.diljith.dhiljith_entrichallenge.network.MovieService


/**
 * Class that handles object creation.
 * Like this, objects can be passed as parameters in the constructors and then replaced for
 * testing, where needed.
 */
object Injection {

    /**
     * Creates an instance of [MovieRepository] based on the [GithubService] and a
     * [GithubLocalCache]
     */
    private fun provideGithubRepository(context: Context): MovieRepository {
        return MovieRepository(MovieService.create(), MovieDatabase.getInstance(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithubRepository(context))
    }
}
