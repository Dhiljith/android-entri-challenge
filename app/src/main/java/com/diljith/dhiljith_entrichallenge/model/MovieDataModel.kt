/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.codelabs.paging.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Immutable model class for a Github repo that holds all the information about a repository.
 * Objects of this type are received from the Github API, therefore all the fields are annotated
 * with the serialized name.
 * This class also defines the Room repos table, where the repo [id] is the primary key.
 */
@Parcelize
@Entity(tableName = "movie_db")
data class MovieDataModel(
    @PrimaryKey @field:SerializedName("id") val id: Int,



    @field:SerializedName("original_language") val original_language: String,
    @field:SerializedName("original_title") val original_title: String,
    @field:SerializedName("overview") val overview: String,
    @field:SerializedName("popularity") val popularity: Double,
    @field:SerializedName("poster_path") val poster_path: String?=null,
    @field:SerializedName("release_date") val release_date: String,
    @field:SerializedName("title") val title: String,

    @field:SerializedName("vote_average") val vote_average: Double,
    @field:SerializedName("vote_count") val vote_count: Int
) : Parcelable

/* "popularity":124.493,
         "vote_count":44,
         "video":false,
         "poster_path":"\/3eg0kGC2Xh0vhydJHO37Sp4cmMt.jpg",
         "id":531499,
         "adult":false,
         "backdrop_path":"\/zogWnCSztU8xvabaepQnAwsOtOt.jpg",
         "original_language":"en",
         "original_title":"The Tax Collector",
         "genre_ids":[
            28,
            80,
            18,
            53
         ],
         "title":"The Tax Collector",
         "vote_average":6,
         "overview":"David Cuevas is a family man who works as a gangland tax collector for high ranking Los Angeles gang members. He makes collections across the city with his partner Creeper making sure people pay up or will see retaliation. An old threat returns to Los Angeles that puts everything David loves in harm’s way.",
         "release_date":"2020-08-07"*/
