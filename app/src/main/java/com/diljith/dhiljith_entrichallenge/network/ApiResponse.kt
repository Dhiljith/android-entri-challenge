package com.diljith.dhiljith_entrichallenge.network

import com.example.android.codelabs.paging.model.MovieDataModel
import com.google.gson.annotations.SerializedName

data class ApiResponse(
    @field:SerializedName("page")val page: Int,
    @field:SerializedName("results")val results: List<MovieDataModel> = emptyList(),
    @field:SerializedName("total_pages")val total_pages: Int,
    @field:SerializedName("total_results")val total_results: Int,
    val nextPage: Int? = null
)
