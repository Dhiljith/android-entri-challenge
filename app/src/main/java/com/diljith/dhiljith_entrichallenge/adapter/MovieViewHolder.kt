
package com.diljith.dhiljith_entrichallenge.adapter

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.diljith.dhiljith_entrichallenge.R
import com.example.android.codelabs.paging.model.MovieDataModel
import com.squareup.picasso.Picasso

/**
 * View Holder for a [MovieDataModel] RecyclerView list item.
 */
class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val name: TextView = view.findViewById(R.id.repo_name)
    private val description: TextView = view.findViewById(R.id.repo_description)
    private val stars: TextView = view.findViewById(R.id.repo_stars)
    private val icPosterImage :ImageView=view.findViewById(R.id.ic_movie_poster)
    private val language: TextView = view.findViewById(R.id.repo_language)
    private val forks: TextView = view.findViewById(R.id.repo_forks)

    private val base_url="https://image.tmdb.org/t/p/w500"

    private var movieDataModel: MovieDataModel? = null


    init {
        view.setOnClickListener {

            click?.onClick(movieDataModel!!)
            /*repo?.title?.let { url ->


            }*/
        }
    }

    fun bind(movieDataModel: MovieDataModel?) {
        if (movieDataModel == null) {
            val resources = itemView.resources
            name.text = resources.getString(R.string.loading)
            description.visibility = View.GONE
            language.visibility = View.GONE
            stars.text = resources.getString(R.string.unknown)
            forks.text = resources.getString(R.string.unknown)
        } else {
            showRepoData(movieDataModel)
        }
    }

    private fun showRepoData(movieDataModel: MovieDataModel) {
        this.movieDataModel = movieDataModel
        name.text = movieDataModel.title

        // if the description is missing, hide the TextView
        var descriptionVisibility = View.GONE
        if (movieDataModel.overview != null) {
            description.text = movieDataModel.overview
            descriptionVisibility = View.VISIBLE
        }
        description.visibility = descriptionVisibility

        stars.text = movieDataModel.vote_count.toString()
        forks.text = movieDataModel.vote_average.toString()

       var image_source = movieDataModel.poster_path?.replace("\\/", "/");

        Picasso.get()
            .load(base_url+image_source)
            .error(R.drawable.ic_picture)
            .placeholder(R.drawable.ic_picture)
            .into(icPosterImage)



        // if the language is missing, hide the label and the value
        var languageVisibility = View.GONE
        /*if (!repo.adult.isNullOrEmpty()) {
            val resources = this.itemView.context.resources
            //
            languageVisibility = View.VISIBLE
        }*/
        language.visibility = languageVisibility
    }

    companion object {

        private lateinit var click: Click

        fun create(parent: ViewGroup, clc: Click): MovieViewHolder {

            click=clc
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.repo_view_item, parent, false)
            return MovieViewHolder(view)
        }
    }

    interface Click{
       fun onClick(movieDataModel: MovieDataModel)
    }
}
