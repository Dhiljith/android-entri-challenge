package com.diljith.dhiljith_entrichallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.diljith.dhiljith_entrichallenge.databinding.ActivityMovieDetailsBinding
import com.example.android.codelabs.paging.model.MovieDataModel
import com.squareup.picasso.Picasso

class MovieDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMovieDetailsBinding
    private val base_url="https://image.tmdb.org/t/p/original"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        binding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        getBundle()
    }

    fun getBundle()
    {
        val bundle=intent.extras
        var movieDataModel: MovieDataModel? = bundle?.getParcelable("data")

        if (movieDataModel != null) {
            binding.tvMovieTitle.text=movieDataModel.original_title
            binding.tvOverView.text=movieDataModel.overview
            binding.tvLan.text="Lang :"+movieDataModel.original_language
            binding.tvReleaseDate.text="Release date :"+movieDataModel.release_date


            var image_source = movieDataModel.poster_path?.replace("\\/", "/");

            Picasso.get()
                .load(base_url+image_source)
                .error(R.drawable.ic_picture)
                .placeholder(R.drawable.ic_picture)
                .into(binding.icPoster)
        }
    }
}