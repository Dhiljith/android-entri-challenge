
package com.diljith.dhiljith_entrichallenge.database

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.android.codelabs.paging.model.MovieDataModel

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(movieDataModels: List<MovieDataModel>)

    @Query("SELECT * FROM movie_db " +
            "ORDER BY popularity DESC")
    fun reposByName(): PagingSource<Int, MovieDataModel>

    @Query("DELETE FROM movie_db")
    suspend fun clearRepos()

}