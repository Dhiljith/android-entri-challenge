
package com.diljith.dhiljith_entrichallenge.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.android.codelabs.paging.db.RemoteKeys
import com.example.android.codelabs.paging.db.RemoteKeysDao
import com.example.android.codelabs.paging.model.MovieDataModel

@Database(
        entities = [MovieDataModel::class, RemoteKeys::class],
        version = 1,
        exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun reposDao(): MovieDao
    abstract fun remoteKeysDao(): RemoteKeysDao

    companion object {

        @Volatile
        private var INSTANCE: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        MovieDatabase::class.java, "MovieDB2.db")
                        .build()
    }
}
