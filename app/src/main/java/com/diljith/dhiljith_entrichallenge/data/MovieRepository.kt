package com.diljith.dhiljith_entrichallenge.data

import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.diljith.dhiljith_entrichallenge.database.MovieDatabase
import com.diljith.dhiljith_entrichallenge.network.MovieService


import com.example.android.codelabs.paging.model.MovieDataModel
import kotlinx.coroutines.flow.Flow

/**
 * Repository class that works with local and remote data sources.
 */
class MovieRepository(
    private val service: MovieService,
    private val database: MovieDatabase
) {

    /**
     * Search repositories whose names match the query, exposed as a stream of data that will emit
     * every time we get more data from the network.
     */
    fun getSearchResultStream(query: String): Flow<PagingData<MovieDataModel>> {
        Log.d("GithubRepository", "New query: $query")

        val pagingSourceFactory = { database.reposDao().reposByName() }

        return Pager(
                config = PagingConfig(pageSize = NETWORK_PAGE_SIZE, enablePlaceholders = false),
                remoteMediator = GithubRemoteMediator(
                        query,
                        service,
                        database
                ),
                pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }
}
